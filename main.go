package main

import (
	"archive/zip"
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	genderInfos, ageInfos, err := readGenderAndAge()
	if err != nil {
		log.Fatalln(err)
	}

	genderCSV, err := os.OpenFile("gender_info.csv", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0664)
	if err != nil {
		log.Fatalln(err)
	}
	defer genderCSV.Close()

	ageCSV, err := os.OpenFile("age_info.csv", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0664)
	if err != nil {
		log.Fatalln(err)
	}
	defer ageCSV.Close()

	fmt.Fprintln(genderCSV, "predicted_gender,prob,day_pt,prob_male,prob_female,prob_enby,model_version")
	fmt.Fprintln(ageCSV, "predicted_age,prob,day_pt,prob_13_17,prob_18_24,prob_25_34,prob_35_over,model_version")

	for _, info := range genderInfos {
		fmt.Fprintf(genderCSV, "%s,%s,%s,%s,%s,%s,%s\n", info.PredictedGender, info.Probability, strings.Split(info.DayPT, " ")[0], info.ProbabilityMale, info.ProbabilityFemale, info.ProbabilityEnby, strings.Split(info.ModelVersion.String(), " ")[0])
	}
	log.Println("Wrote gender_info.csv.")

	for _, info := range ageInfos {
		fmt.Fprintf(ageCSV, "%s,%s,%s,%s,%s,%s,%s,%s\n", info.PredictedAge, info.Probability, strings.Split(info.DayPT, " ")[0], info.Prob13_17, info.Prob18_24, info.Prob25_34, info.Prob35_over, strings.Split(info.ModelVersion.String(), " ")[0])
	}
	log.Println("Wrote age_info.csv.")
}

func readGenderAndAge() (genderInfos []GenderInfo, ageInfos []AgeInfo, err error) {
	zip, err := zip.OpenReader("./package.zip")
	if err != nil {
		return nil, nil, fmt.Errorf("opening zip (did you download your Discord package.zip and put it in the current directory?): %w", err)
	}
	defer zip.Close()

	events, futureErr := streamEvents(zip.File)

	for event := range events {
		if strings.Contains(event, `"predicted_gender"`) {
			var info GenderInfo
			err = json.Unmarshal([]byte(event), &info)
			if err != nil {
				return
			}
			genderInfos = append(genderInfos, info)
		}

		if strings.Contains(event, `"predicted_age"`) {
			var info AgeInfo
			err = json.Unmarshal([]byte(event), &info)
			if err != nil {
				return
			}
			ageInfos = append(ageInfos, info)
		}
	}

	if *futureErr != nil {
		err = *futureErr
		return
	}

	return
}

func streamEvents(files []*zip.File) (ch chan string, futureErr *error) {
	ch = make(chan string)
	futureErr = new(error)

	go func() {
		defer close(ch)

		for _, file := range files {
			if strings.HasPrefix(file.Name, "activity/analytics/") && strings.Contains(file.Name, "events") {
				log.Printf("Found %s.", file.Name)
				f, err := file.Open()
				if err != nil {
					*futureErr = err
				}
				defer f.Close()

				r := bufio.NewReader(f)
				for {
					line, err := r.ReadString('\n')
					if err != nil {
						if err == io.EOF {
							break
						}
						*futureErr = err
						return
					}

					ch <- line
				}
			}
		}
	}()

	return
}
