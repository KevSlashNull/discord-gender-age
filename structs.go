package main

import (
	"time"
)

type Float string

func (f *Float) UnmarshalJSON(b []byte) error {
	*f = Float(string(b))
	return nil
}

func (f Float) String() string {
	return string(f)
}

type AgeInfo struct {
	UserID       string    `json:"user_id"`
	PredictedAge string    `json:"predicted_age"`
	Probability  Float     `json:"probability"`
	Prob13_17    Float     `json:"prob_13_17"`
	Prob18_24    Float     `json:"prob_18_24"`
	Prob25_34    Float     `json:"prob_25_34"`
	Prob35_over  Float     `json:"prob_35_over"`
	ModelVersion time.Time `json:"model_version"`
	DayPT        string    `json:"day_pt"`
}

type GenderInfo struct {
	UserID            string    `json:"user_id"`
	PredictedGender   string    `json:"predicted_gender"`
	Probability       Float     `json:"probability"`
	ProbabilityMale   Float     `json:"prob_male"`
	ProbabilityFemale Float     `json:"prob_female"`
	ProbabilityEnby   Float     `json:"prob_non_binary_gender_expansive"`
	ModelVersion      time.Time `json:"model_version"`
	DayPT             string    `json:"day_pt"`
}
