## Discord gender/age prediction extractor

Extracts age and gender predictions from your Discord data export
(aka. `package.zip`) into two CSV files: `age_info.csv` and
`gender_info.csv`

## How to run

1. Request a Discord data export if you haven’t
1. Download `package.zip` from your data export and put it into this directory
1. Install [Go](https://go.dev), if you haven’t
1. Run `go run .`
1. `age_info.csv` and `gender_info.csv` will be created
